using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Allows the player to, you know, proceed to the next level
/// </summary>
public class LevelEnd : MonoBehaviour
{
    [SerializeField] private Canvas gameWinUI;
    [SerializeField] private Image fadeTransitionImage;

    private float fadingTime = 1f;
    private bool fadeOut = true;

    /// <summary>
    /// Disbles the victory UI.
    /// </summary>
    private void Start()
    {
        gameWinUI.enabled = false;
        StartCoroutine(Fade());
    }

    /// <summary>
    /// After the game is won, checks if the player is preessing "N" and activates the next level if he is.
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N) && gameWinUI.enabled == true)
        {
            StartCoroutine(Fade());
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    /// <summary>
    /// Upon colision, checks if its a player and anebles the victory UI if it is.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gameWinUI.enabled = true;
        }
    }

    /// <summary>
    /// The scene transition
    /// </summary>
    private IEnumerator Fade()
    {
        if (fadeOut)
        {
            fadeTransitionImage.CrossFadeAlpha(0f, fadingTime, true);
            fadeOut = false;
        }
        else
        {
            fadeTransitionImage.raycastTarget = true;
            fadeTransitionImage.CrossFadeAlpha(1f, fadingTime, true);
            yield return new WaitForSeconds(fadingTime*1.5f);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    /// <summary>
    /// Resets the amount of bushes.
    /// </summary>
    void Start()
    {
        PlayerPrefs.SetInt("BushesTouched", 0);
    }
}

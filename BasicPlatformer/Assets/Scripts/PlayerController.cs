using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The stuff to control the player
/// </summary>
public class PlayerController : MonoBehaviour
{
    [SerializeField] private float playerSpeed = 5.0f;
    [SerializeField] private float jumpPower = 5.0f;
    [SerializeField] private int jumpsMaximum;
    [SerializeField] private int jumpsLeft;
    private Rigidbody2D _playerRigidbody;

    /// <summary>
    /// Selects the rigidbody and allows its usage
    /// </summary>
    private void Start()
    {
        _playerRigidbody = GetComponent<Rigidbody2D>();
        if (_playerRigidbody == null)
        {
            Debug.LogError("Player is missing a Rigidbody2D component");
        }
    }

    /// <summary>
    /// calls the script to move the player right and left, adds him the ability to double jump, and essentially caps the speed. also teleports him to th beggining of the level if he fails.
    /// </summary>
    private void Update()
    {
        MovePlayer();
        if (Input.GetKeyDown(KeyCode.Space) && jumpsLeft > 0 && _playerRigidbody.velocity.y < 10)
        {
            _playerRigidbody.velocity += new Vector2(_playerRigidbody.velocity.x, jumpPower);
            jumpsLeft--;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && jumpsLeft > 0)
        {
            _playerRigidbody.velocity = new Vector2(_playerRigidbody.velocity.x, jumpPower);
            jumpsLeft--;
        }

        if (transform.position.y < -10)
        {
            transform.position = new Vector2(-4, -3);
        }
    }

    /// <summary>
    /// Allows the player to move right and left
    /// </summary>
    private void MovePlayer()
    {
        var horizontalInput = Input.GetAxisRaw("Horizontal");
        _playerRigidbody.velocity = new Vector2(horizontalInput * playerSpeed, _playerRigidbody.velocity.y);
    }

    /// <summary>
    /// Checks if the player can double jump
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            jumpsLeft = jumpsMaximum;
        }
    }
}
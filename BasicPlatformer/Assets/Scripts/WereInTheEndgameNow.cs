using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WereInTheEndgameNow : MonoBehaviour
{
    /// <summary>
    /// If you touched enough bushes, changes to the next scene. Why am I doing this again?
    /// </summary>
    void Update()
    {
        if (PlayerPrefs.GetInt("BushesTouched") >= 32)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}

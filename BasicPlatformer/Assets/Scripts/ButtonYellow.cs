using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The script used by all buttons and pressure plates. Ignore the "Yellow" part.
/// </summary>
public class ButtonYellow : MonoBehaviour
{
    [SerializeField] private GameObject[] platformActivation;
    /// <summary>
    /// Deactivates the platforms at the beggining of the game.
    /// </summary>
    private void Start()
    {
        foreach (GameObject item in platformActivation)
        {
            item.SetActive(false);
        }
    }
    /// <summary>
    /// Checks if the button or pressure plate collides with something, and if it is a player it activates the previously deactivated platforms if it is.
    /// </summary>
    /// <param name="collision">Usually the player</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {            
            foreach (GameObject item in platformActivation)
            {
                item.SetActive(true);
            }
            Destroy(gameObject);

        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Just as a heads up, this script was copied in its entirty from a previous game i worked on. Hopr that explains why its so bad.
/// </summary>
public class BossMovement : MonoBehaviour
{
    public GameObject Player;
    public GameObject Boss1;
    public GameObject projectile;
    public List<float> PatrolPositions = new List<float>();     // Positions de patrouille
    public bool GotThere;
    public float Speed;                     // Vitesse de la patrouille
    public int Damage;                      // Dommage inflig� au joueur
    public int Health;                      // Vie de l'ennemie
    public Slider BossHpBar;

    private int _currentPatrolIndex = 1;    // Index de la position dans la patrouille

    private bool _reverse;

    private Vector3 pos;

    private void Start()
    {
        ProjectileSpawn();

        InvokeRepeating("ProjectileSpawn", 0, 2);
        BossHpBar.value = Health;
    }

    private void Update()
    {
        if (transform.position.x != PatrolPositions[_currentPatrolIndex])
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector2(PatrolPositions[_currentPatrolIndex], transform.position.y), Time.deltaTime * Speed);
        }
        else
        {
            if (_reverse == false)
            {
                if (_currentPatrolIndex + 1 >= PatrolPositions.Count)
                {
                    _reverse = true;
                }
                else
                {
                    _currentPatrolIndex++;
                }
            }
            else
            {
                if (_currentPatrolIndex - 1 < 0)
                {
                    _reverse = false;
                }
                else
                {
                    _currentPatrolIndex--;
                }
            }
        }


        pos = new Vector3(Boss1.transform.position.x, Boss1.transform.position.y, 0);
    }

    public void ProjectileSpawn()
    {

        for (int i = 0; i < 360; i += 30)
        {
            Instantiate(projectile, Boss1.transform.position, Quaternion.Euler(0, 0, i));
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            BossHpBar.value -= 1;
            if (BossHpBar.value <= 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
    }
}

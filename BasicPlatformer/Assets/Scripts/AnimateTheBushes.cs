using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateTheBushes : MonoBehaviour
{
    private float offset = 0.25f;
    private bool hasBeenTouched = false;

    /// <summary>
    /// reduces the size of the bushes, if you enter them.
    /// </summary>
    /// <param name="collision">The player, usually</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            transform.localScale = new Vector3(1,0.5f,1);
            transform.position = transform.position + new Vector3(0, -offset, 0);
        }
            
    }

    /// <summary>
    /// increases the size of the bushes, if you exit.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            transform.localScale = Vector3.one;
            transform.position = transform.position + new Vector3(0, offset, 0);
            SuperSecretBoss();
        }
    }

    /// <summary>
    /// increases the counter to maybe activate the final boss.
    /// </summary>
    private void SuperSecretBoss()
    {
        if (!hasBeenTouched)
        {
            PlayerPrefs.SetInt("BushesTouched", PlayerPrefs.GetInt("BushesTouched") + 1);
            hasBeenTouched = true;
        }
    }
}

# platformer

# journal de bord

## 4 novembre 2022

- création du répositoitre
- répartition des tâches
- update du readme

## 11 novembre 2022
<ul>
<li>Natan a fait de la recherche sur la partie online.</li>
<li>Tomass a avance dans le jeu
<ol>
<li>il a ajoute un joueur qui bouge</li>
<li>il a ajouté un ciel</li>
<li>il a ajouté beaucoup de textures</li>
<li>il a ajouté la majorité du premier niveau</li>
</ol>
</li>
<li>on a ajouté une license</li>
</ul>

## 18 novembre 2022
<ul>
<li>Natan a eu une crise existentielle et a travaille sur un project a part</li>
<li>Tomass a travaille sur le jeu en ajoutant:
<ol>
<li>un systeme de plaques de pression</li>
<li>une mechanique de grimpe aux mures</li>
<li>un moyen de coller au dessous d'une platforme</li>
</ol>
<li>a motive natan a travailler</li>
<li>a delete <em>BEAUCOUP(30000+)</em> de fichiers inutiles/nocifs dans le git</li>
<li>a update le README</li>
</li>

</ul>

## 25 novembre 2022
<ul>
<li>Natan etait absent.</li>
<li>Tomass a continue a travailler sur le jeu.</li>
<li>il a ajoute le 2eme, 3eme et commence le 4eme niveaux</li>
<li>il a ajoute un UI de victoire</li>
<li>il a ajoute un moyen de changer enre ls niveaux</li>
<li>il a ajoute des tips pour les joueurs</li>
</ul>

## 2 et 9 December 2022
<ul>
<li>Fix de plusieurs bugs, notament de la dispaition de la skybox.</li>
<li>Ajout des derniers niveaux</li>
<li>Ajout de la fin du jeux</li>
<li>debut du travail sur les transitions entre les niveaux</li>
</ul>

## 16 December 2022
<ul>
<li>Ajout des buissons</li>
<li>Ajout d'une arene de bosse secrete</li>
<li>Ajout des transitions entre les niveaux</li>
</ul>

## 23 December 2022
<ul>
<li>Ajout d'une maniere de quitter le jeu</li>
<li>Creation du premier build</li>
</ul>

# description

Ce ci est un jeux video de cooperation.
l'idée et le design de ce jeu est, pour resumer:
<ol>
<li> Ce n'est pas tou les jours que les deux joueurs arrivent a la fin, mais tous les joueurs passent au prochain niveau.</li>
<li> Tous les niveaux sont possibles, mais pas tous faciles.</li>
</ol>
à moi meme, élaborer sur ce theme

# participents

Les personnes qui travaillent sur ce project sont:
<ul>
<li>Tomass</li>
<li>Natan</li>
</ul>

# fonctionalité
ce jeux fontionne en combinant unity et donc C#, et les capacités de python de connection entre ordinateurs.

